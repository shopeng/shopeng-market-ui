var app = angular.module("myApp", ['ui.router', 'ngSanitize']);
app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $urlRouterProvider.otherwise('/');
            // UI Router States
            // Inserting Page title as State Param
            $stateProvider
                  .state('home', {
                        url: '/',
                        templateUrl: 'partials/home.html',
                        params: {
                              title: "Login to AFES"
                        }
                  })
               .state('login', {
                  url: '/login',
                  templateUrl: 'partials/login.html',
                  params: {
                     title: "Login to AFES"
                  }
               })
               .state('register', {
                  url: '/register',
                  templateUrl: 'partials/register.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               .state('wizard', {
                  url: '/wizard',
                  templateUrl: 'partials/wizard.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               
               .state('schedule', {
                  url: '/schedule',
                  templateUrl: 'partials/schedule.html',
                  params: {
                     title: "Login to AFES"
                  }
               })
               .state('sponsors', {
                  url: '/sponsors',
                  templateUrl: 'partials/sponsors.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               .state('users', {
                  url: '/users',
                  templateUrl: 'partials/users.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               .state('abstract', {
                  url: '/abstract',
                  templateUrl: 'partials/abstract.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               .state('gallery', {
                  url: '/gallery',
                  templateUrl: 'partials/gallery.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               
               .state('notification', {
                  url: '/notification',
                  templateUrl: 'partials/notification.html',
                  params: {
                     title: "Login to AFES"
                  }
               })


               .state('settings', {
                  url: '/settings',
                  templateUrl: 'partials/settings.html',
                  params: {
                     title: "Login to AFES"
                  }
               })


               

               .state('super-admin', {
                  url: '/super-admin',
                  templateUrl: 'partials/super-admin.html',
                  params: {
                     title: "Login to AFES"
                  }
               })

               
               
               
               
               
               .state('invoice', {
                  url: '/invoice',
                  templateUrl: 'partials/invoice.html',
                  params: {
                     title: "Login to AFES"
                  }
               })


            $locationProvider.html5Mode({ enabled: true, requireBase: false });

      })
     
      .controller('AppController',
       function ($scope, $state, $stateParams, $rootScope, $location, $http, $sce) {
         
            $scope.currentState = false;
         $scope.updateTitle = function (state, pre) {
            $scope.currentState = $state.current.name
         }


         
         
      $http.get("partials/pageload.htm")
         .then(function(response) {
             $scope.myWelcome = response.data;
             
         });
        


            $scope.loadDoc=function() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML =
                this.responseText;
              }
            };
            xhttp.open("GET", "partials/myfile.htm", true);
            xhttp.send();
          }
     



         $rootScope.$on('$stateChangeSuccess', $scope.updateTitle);
         $scope.pageCheck = function(){
            console.log($scope.currentState);
            if ($scope.currentState == 'login' || $scope.currentState == 'register' || $scope.currentState == 'wizard'){
               return false;
            }else{
               return true;    
            }
         }
         $scope.showPage = function(id){
            $('#' + id).toggleClass('hide');
            var body = $("html, body");
            body.stop().animate({ scrollLeft: body.width() - body.scrollLeft() }, 600, function () {
            });
           // $(document).scrollLeft($(document).width() - $(document).scrollLeft())
           
         }
      })

      
      